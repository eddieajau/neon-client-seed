/**
 * @copyright 2018 Contributors. All rights reserved.
 * @license Apache - 2.0
 */

export class HelloWorld extends HTMLElement {
	public connectedCallback() {
		this.render();
	}

	public render() {
		this.innerHTML = '<h1>Hello World!</h1>';
	}
}

customElements.define('hello-world', HelloWorld);
