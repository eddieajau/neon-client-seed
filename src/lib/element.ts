/**
 * @copyright 2018 Contributors. All rights reserved.
 * @license Apache - 2.0
 */

export function element(tag: string): HTMLElement;
export function element(tag: string, inner: string): HTMLElement;
export function element(tag: string, attributes: { [key: string]: string }, inner?: string): HTMLElement;
export function element(tag: string, attributes: { [key: string]: string } | string = {}, inner?: string): HTMLElement {
	const el = document.createElement(tag);
	if (typeof attributes === 'string') {
		inner = attributes;
		attributes = {};
	}
	Object.keys(attributes).forEach(attr => {
		el.setAttribute(attr, attributes[attr]);
	});
	if (!!inner) {
		el.innerHTML = inner;
	}
	return el;
}
