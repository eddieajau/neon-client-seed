const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
	mode: 'development',

	entry: [
		'./dist/src/index.js',
		'./src/styles/index.scss'
	],

	output: {
		path: path.resolve(__dirname, 'public'),
		filename: 'index.js'
	},

	module: {
		rules: [{
			test: /\.(sass|scss)$/,
			loader: ExtractTextPlugin.extract(['css-loader', 'sass-loader'])
		}]
	},

	plugins: [
		new ExtractTextPlugin({
			filename: 'index.css',
			allChunks: true
		})
	]
};